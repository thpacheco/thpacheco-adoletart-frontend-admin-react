import { Anchor, Box } from "grommet";

const FooterComponent = () => {
    return (<Box tag="footer" pad="small" background="dark-1">
        <Anchor color="white" size="small" href="">
            @Adoletarts Todos direitos
        </Anchor>
    </Box>);
}

export default FooterComponent;